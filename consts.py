DEFAULT_PARAMETERS = {
    'page_parameter_name': 'page',
    'page_size_parameter_name': 'page_size',
    'sort_parameter_name': 'sort',
    'sort_order_parameter_name': 'order',
}

DEFAULT_HEADERS = {
    'page-count': 'Item-Count',
    'error-message': 'Error-Message',
    'error-code': 'Error-Code',
}

RESPONSE_CODE = {
    0: 'NORMAL',
    100: 'test'
}
