from flask import Blueprint, jsonify
from utils.base_urls import s_param, s_request
from utils.strings import to_bool

live = Blueprint('live', __name__, url_prefix='/live')


@live.route('/')
def live_list():
    params = s_param()
    if 'recommended' not in params or not to_bool(params['recommended']):
        json_response = s_request('/nbs/list', params)
    else:
        json_response = s_request('/nbs/list/recommended', params)
    result = json_response['response']
    return jsonify(result)


@live.route('/<string:session_id>/statistic', methods=['GET'])
def live_statistic(session_id):
    params = s_param()
    params['session_id'] = session_id
    json_response = s_request('/nbs/statistics', params)
    result = json_response['response']
    return jsonify(result)


@live.route('/<int:account_id>/recommend/', methods=['POST'])
def live_recommend(account_id):
    params = s_param(host_id=account_id)
    json_response = s_request('/nbs/recommend', params, method='POST')
    print json_response
    result = json_response['response']
    return jsonify(result)


@live.route('/<int:account_id>/un-recommend/', methods=['POST'])
def live_un_recommend(account_id):
    params = s_param(host_id=account_id)
    json_response = s_request('/nbs/unrecommend', params, method='POST')
    print json_response
    result = json_response['response']
    return jsonify(result)


@live.route('/<string:session_id>', methods=['PUT'])
def live_update(session_id):
    params = s_param()
    params['session_id'] = session_id
    json_response = s_request('/nbs/update', params)
    result = json_response['response']
    return jsonify(result)


@live.route('/<string:session_id>/stop', methods=['POST'])
def live_stop(session_id):
    params = s_param()
    params['session_id'] = session_id
    json_response = s_request('/nbs/stop', params)
    result = json_response
    return jsonify(result)
