from datetime import datetime

from peewee import *
from db import myDB
from blueprints.admin.model import Admin


class OperationLog(Model):
    id = PrimaryKeyField()
    operator = ForeignKeyField(Admin)
    type = IntegerField(default=0)
    target_id = CharField(default='')
    target_type = CharField(default='')
    create_time = DateTimeField(default=datetime.now())
    detail = TextField()

    class Meta:
        database = myDB
        db_table = 'operation_log'


if not OperationLog.table_exists():
    OperationLog.create_table()
