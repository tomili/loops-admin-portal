from flask import Blueprint
from model import OperationLog
from utils.Pagination import add_pagination_params, add_filter_params, add_sort_params
from utils.base_urls import s_response

operation_log = Blueprint('operation_log', __name__, url_prefix='/operation')


@operation_log.route('/')
def operation_list():
    query = OperationLog.select()
    query = add_filter_params(query, OperationLog)
    query = add_sort_params(query, OperationLog)
    query, count = add_pagination_params(query)
    result = query.execute()
    return s_response(result, count)
