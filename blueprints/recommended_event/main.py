from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request

recommended_event = Blueprint('recommended_event', __name__, url_prefix='/recommended-event')


@recommended_event.route('/', methods=['GET'])
def event_recommended_list():
    params = s_param()
    json_response = s_request('/topevent/index', params, method='GET')
    result = json_response.get('response')
    return jsonify(result)


@recommended_event.route('/', methods=['POST'])
def event_recommended_list_update():
    params = s_param()
    json_response = s_request('/topevent/update', params, method='POST')
    result = json_response.get('response')
    return jsonify(result)
