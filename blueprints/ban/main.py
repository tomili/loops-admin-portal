from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

ban = Blueprint('ban', __name__, url_prefix='/banned-user')


@ban.route('/')
def ban_list():
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    json_response = s_request('/ban/index', params)
    result, count = json_response['response']['result'], json_response['response']['total_record_count']
    return s_response(result, item_count=count)
