from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

tag = Blueprint('tag', __name__, url_prefix='/tag')


@tag.route('/', methods=['GET'])
def tag_list():
    params = s_param()
    json_response = s_request('/tag/get-all', params)
    result = json_response['recommendTagList']
    return s_response(result)


@tag.route('/', methods=['POST'])
def tag_create():
    params = s_param()
    s_request('/tag/create', params, method='POST')
    return jsonify({})


@tag.route('/<int:tag_id>/', methods=['PUT'])
def tag_edit(tag_id):
    params = s_param()
    params['id'] = tag_id
    s_request('/tag/update', params, method='POST')
    return jsonify()


@tag.route('/<int:tag_id>/', methods=['DELETE'])
def tag_delete(tag_id):
    params = s_param()
    params['id'] = tag_id
    s_request('/tag/delete', params, method='POST')
    return jsonify({})


@tag.route('/<int:tag_id>/', methods=['GET'])
def tag_detail(tag_id):
    params = s_param()
    params['id'] = tag_id
    json_response = s_request('/tag/detail', params, method='GET')
    result = json_response['recommendTag']
    return jsonify(result)
