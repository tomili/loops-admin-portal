from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

transaction = Blueprint('transaction', __name__, url_prefix='/transaction')


@transaction.route('/diamonds-to-cash/', methods=['GET'])
def diamonds_to_cash_list():
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    json_response = s_request('/treasure/cash-out/details', params)
    result = json_response.get('response').get('result')
    return jsonify(result)


@transaction.route('/diamonds-to-cash/<string:transaction_id>/process/', methods=['POST'])
def diamonds_to_cash_process(transaction_id):
    params = s_param()
    params['transactionId'] = transaction_id
    json_response = s_request('/treasure/cash-out/process', params)
    result = json_response.get('response').get('result')
    return jsonify(result)


@transaction.route('/diamonds-to-cash/', methods=['POST'])
def diamonds_to_coins_create():
    params = s_param()
    json_response = s_request('/treasure/diamonds-to-cash', params)
    print json_response
    # result = json_response.get('response').get('result')
    return jsonify(json_response)


@transaction.route('/diamonds-to-coins/', methods=['GET'])
def diamonds_to_coins_list():
    params = s_param()
    json_response = s_request('/treasure/diamonds-to-coins/details', params)
    # result = json_response.get('response').get('result')
    return jsonify(json_response)


@transaction.route('/diamonds-to-cash-history/', methods=['GET'])
def diamonds_to_cash_history():
    params = s_param()
    if 'user_id' in params and 'month' not in params and 'year' not in params:
        json_response = s_request('/treasure/income/history')
    elif 'user_id' in params and 'month' in params and 'year' in params:
        json_response = s_request('/treasure/income/history-detail')
    elif 'user_id' not in params and 'month' in params and 'year' in params:
        json_response = s_request('/treasure/income/gen-history')
    return jsonify(json_response)
