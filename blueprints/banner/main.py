from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

banner = Blueprint('banner', __name__, url_prefix='/banner')


@banner.route('/', methods=['GET'])
def banner_list():
    params = s_param(page_size_param="pageSize", page_param="pageNumber")
    json_response = s_request('/adv/index', params, method='POST')
    result, count = json_response.get('response').get('result'), json_response.get('response').get('total_record_count')
    return s_response(result, item_count=count)


@banner.route('/', methods=['POST'])
def banner_create():
    params = s_param()
    json_response = s_request('/adv/create', params, method='POST')
    result = json_response['response']
    return jsonify(result)


@banner.route('/<int:banner_id>/', methods=['GET'])
def banner_detail(banner_id):
    params = s_param()
    params['id'] = banner_id
    json_response = s_request('/adv/detail', params, method='GET')
    result = json_response['response']
    return jsonify(result)


@banner.route('/<int:banner_id>/', methods=['DELETE'])
def banner_delete(banner_id):
    params = s_param()
    params['id'] = banner_id
    json_response = s_request('/adv/delete', params, method='POST')
    result = json_response['response']
    return jsonify(result)


@banner.route('/<int:banner_id>/', methods=['PUT'])
def banner_edit(banner_id):
    params = s_param()
    params['id'] = banner_id
    json_response = s_request('/adv/update', params, method='POST')
    result = json_response['response']
    return jsonify(result)


"""
Float Banner
"""


@banner.route('/float/', methods=['GET'])
def float_banner_list():
    params = s_param(isNotDeleted=False)

    if 'activityType' not in params:
        json_response = s_request('/float-banner/get-all', params)
    else:
        json_response = s_request('/float-banner/get-by-type', params)

    result = json_response['floatBanners']
    return jsonify(result)


@banner.route('/float/<int:float_banner_id>/', methods=['GET'])
def float_banner_detail(float_banner_id):
    params = s_param(bannerId=float_banner_id)
    json_response = s_request('/float-banner/get-by-id', params)
    result = json_response['floatBanner']
    return jsonify(result)


@banner.route('/float/<int:float_banner_id>/', methods=['DELETE'])
def float_banner_delete(float_banner_id):
    params = s_param(bannerId=float_banner_id)
    json_response = s_request('/float-banner/delete-by-id', params, method='POST')
    result = json_response
    return jsonify(result)


@banner.route('/float/', methods=['POST'])
def float_banner_create():
    params = s_param()
    json_response = s_request('/float-banner/add', params, method='POST')
    result = json_response
    return jsonify(result)


@banner.route('/float/<int:float_banner_id>/', methods=['PUT'])
def float_banner_edit(float_banner_id):
    params = s_param(bannerId=float_banner_id)
    json_response = s_request('/float-banner/update', params, method='POST')
    result = json_response
    return jsonify(result)
