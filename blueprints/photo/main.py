from flask import Blueprint, request, current_app

from utils.base_urls import s_upload, PHOTO_SERVER, s_response, PHOTO_DOMAIN, s_request, s_param

photo = Blueprint('photo', __name__, url_prefix='/photo')


@photo.route('/', methods=['POST'])
def photo_upload():
    uploaded_file = request.files['file']
    json_response = s_upload('/photo-service/photo', uploaded_file, server=PHOTO_SERVER)
    return s_response({'url': PHOTO_DOMAIN + json_response['url']})


@photo.route('/', methods=['GET'])
def photo_list():
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    json_response = s_request('/photo/index', params)
    result, count = json_response['entityList'], json_response['totalRecordCount']
    return s_response(result, item_count=count)
