from datetime import datetime

from peewee import *
from hashlib import md5

from db import myDB


class Role(Model):
    id = PrimaryKeyField()
    name = CharField(unique=True)
    permissions = CharField(null=True)  # JSON

    class Meta:
        database = myDB
        db_table = 'role'


if not Role.table_exists():
    Role.create_table()
    super_role = Role(
            name='admin',
            permissions=''
    )
    super_role.save()


class Admin(Model):
    id = PrimaryKeyField()
    username = CharField()
    password = CharField()
    create_time = DateTimeField(default=datetime.now())
    update_time = DateTimeField(null=True)
    email = CharField(unique=True)
    description = CharField(null=True)
    role = ForeignKeyField(Role, null=True)
    locked = IntegerField(null=True)
    phone_number = CharField(null=True)
    last_login_time = DateTimeField(null=True)
    last_logout_time = DateTimeField(null=True)
    last_change_password_time = DateTimeField(null=True)
    token = CharField(null=True)

    class Meta:
        database = myDB
        db_table = 'account'


if not Admin.table_exists():
    Admin.create_table()
    role = Role.select().limit(1)[0]

    default_admin = Admin(
            username='admin',
            email='admin@rings.tv',
            role=role,
            password=md5('admin').hexdigest(),
    )
    default_admin.save()
