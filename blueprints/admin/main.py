import uuid
from datetime import datetime
from hashlib import md5
from flask import Blueprint, request, g
from peewee import DoesNotExist
from model import Admin, Role
from utils.Pagination import add_pagination_params
from utils.base_urls import s_param, s_response
from utils.Decorators import login_required
from utils import logger

admin = Blueprint('admin', __name__, url_prefix='/admin')


@admin.route('/login/', methods=['POST'])
def admin_login():
    params = s_param()
    email = params.get('email')
    password = params.get('password')

    print md5(password).hexdigest()
    try:
        result = Admin.get(
            (Admin.email == email) &
            (Admin.password == md5(password).hexdigest())
        )
    except DoesNotExist:
        raise LookupError('Wrong Username or Password')

    result.token = uuid.uuid4()
    result.last_login_time = datetime.now()
    result.save()
    return s_response(result)


@admin.route('/logout/', methods=['POST'])
@login_required
def admin_logout():
    admin_entity = Admin.get(Admin.id == g._current_user_['id'])
    admin_entity.last_logout_time = datetime.now()
    admin_entity.token = None
    admin_entity.save()
    return s_response({})


@admin.route('/<int:admin_id>/update-password', methods=['POST'])
def admin_update_password(admin_id):
    new_password = request.json.get('new_password')
    re_new_password = request.json.get('re_new_password')

    if not new_password:
        raise Exception('new password can not be empty')

    print new_password != re_new_password
    print new_password
    print re_new_password

    if new_password != re_new_password:
        raise Exception('two passwords are not equal')

    admin_model = Admin.get(Admin.id == admin_id)
    admin_model.password = md5(new_password).hexdigest()
    admin_model.save()

    return s_response({})


@admin.route('/')
@login_required
def admin_list():
    query = Admin.select()
    result, count = add_pagination_params(query)
    return s_response(result, item_count=count)


@admin.route('/<int:admin_id>')
def admin_detail(admin_id):
    result = Admin.get(Admin.id == admin_id)
    return s_response(result)


@admin.route('/', methods=['POST'])
def admin_add():
    username = request.json.get('username')
    password = request.json.get('password')
    email = request.json.get('email')
    role = request.json.get('role')

    query = Admin.select().where(Admin.email == email)
    if len(query) > 0:
        raise Exception('User with email \'{}\' already exist'.format(email))

    result = Admin.create(
        username=username,
        password=md5(password).hexdigest(),
        email=email,
        role=role,
    )

    return s_response(result)


@admin.route('/<int:admin_id>', methods=['DELETE'])
def admin_delete(admin_id):
    query = Admin.delete().where(Admin.id == admin_id)
    query.execute()
    return s_response()


@admin.route('/role/', methods=['GET'])
@login_required
def admin_role_list():
    query = Role.select()
    result, count = add_pagination_params(query)
    return s_response(result, item_count=count)


@admin.route('/role/', methods=['POST'])
@login_required
def admin_role_create():
    name = request.json.get('name')
    Role.get_or_create(name=name)
    return s_response()


@admin.route('/role/<int:role_id>/', methods=['DELETE'])
@login_required
def admin_role_remove(role_id):
    role = Role.get(id=role_id)
    role.delete_instance()
    return s_response()


@admin.route('/role/<int:role_id>/', methods=['GET'])
@login_required
def admin_role_get(role_id):
    role = Role.get(id=role_id)
    return s_response(role)


@admin.route('/role/<int:role_id>/', methods=['PUT'])
@login_required
def admin_role_update(role_id):
    params = s_param()
    role = Role.get(id=role_id)
    role.name = params.get('name', role.name)
    role.permissions = params.get('permissions', role.permissions)
    role.save()
    return s_response(role)
