from blueprints.admin.model import Role


def add_role(name, permission=None):
    Role.get_or_create(name=name, permission=permission)
