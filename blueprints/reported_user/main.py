from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

reported_user = Blueprint('reported_user', __name__, url_prefix='/reported-user')


@reported_user.route('/')
def reported_user_list():
    params = s_param(page_param='pageNumber', page_size_param='pageSize')
    json_response = s_request('/abuse/summary', params)
    result, count = json_response['response']['result'], json_response['response']['total_record_count']
    return s_response(result, item_count=count)


@reported_user.route('/<int:user_id>/', methods=['GET'])
def reported_user_detail(user_id):
    params = s_param(targetId=user_id, page_param='pageNumber', page_size_param='pageSize')
    json_response = s_request('/abuse/detail', params, method='GET')
    result, count = json_response['response']['result'], json_response['response']['total_record_count']
    return s_response(result, item_count=count)


@reported_user.route('/<int:user_id>/', methods=['DELETE'])
def reported_user_clear(user_id):
    params = s_param(targetId=user_id)
    s_request('/abuse/clear', params, method='POST')
    return s_response()
