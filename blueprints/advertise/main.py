from flask import Blueprint

from utils.base_urls import s_param, s_request

advertise = Blueprint('advertise', __name__, url_prefix="/advertise")


@advertise.route('/banner/')
def banner_list():
    params = s_param()
    json_result = s_request('/adv/')
