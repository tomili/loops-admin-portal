from flask import Blueprint

from utils.base_urls import s_param, s_request, s_response

feedback = Blueprint('feedback', __name__, url_prefix='/feedback')


@feedback.route('/')
def feedback_list():
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    json_response = s_request('/feedback/index', params)
    result, count = json_response['entityList'], json_response['totalRecordCount']
    return s_response(result, item_count=count)


@feedback.route('/<int:feedback_id>/', methods=['GET'])
def feedback_detail(feedback_id):
    params = s_param(id=feedback_id)
    json_response = s_request('/feedback/get', params)
    result = json_response['feedback']
    return s_response(result)


@feedback.route('/<int:feedback_id>/', methods=['DELETE'])
def feedback_remove(feedback_id):
    params = s_param(id=feedback_id)
    json_response = s_request('/feedback/delete', params, method='POST')
    print json_response
    result = json_response
    return s_response(result)
