from flask import Blueprint

from utils.Decorators import login_required
from utils.base_urls import s_param, s_request, s_response
import arrow

account = Blueprint('account', __name__, url_prefix="/account")


@account.route('/')
@login_required
def account_list():
    params = s_param(page_param='pageNo', page_size_param="pageSize")
    json_result = s_request('/account/index', params)
    result, count = json_result['accounts'], json_result['totalRecordCount']
    return s_response(result, item_count=count)


@account.route('/<int:account_id>/')
@login_required
def account_detail(account_id):
    params = {'id': account_id}
    json_result = s_request('/account/detail', params)
    result = json_result['account']
    return s_response(result)


@account.route('/<int:account_id>/device-info')
def account_device_info(account_id):
    params = s_param()
    params['userId'] = account_id
    json_result = s_request('/account/get-client-info', params, method='POST')
    result = json_result
    return s_response(result)


@account.route('/<int:account_id>/reward/')
def account_reward(account_id):
    params = s_param(page_param='pageNo', page_size_param="pageSize")
    params['userId'] = account_id
    json_result = s_request('/reward/paginate', params, method='POST')
    result = json_result['rewards']
    return s_response(result)


@account.route('/<int:account_id>/level/', methods=['GET'])
def account_level(account_id):
    params = s_param()
    params['id'] = account_id
    json_result = s_request('/account/level/details', params)
    result = json_result['entity']
    return s_response(result)


@account.route('/<int:account_id>/level/', methods=['POST'])
def account_level_set(account_id):
    params = s_param()
    params['userId'] = account_id
    json_result = s_request('/account/level/set-level', params, method='POST')
    result = json_result
    return s_response(result)


@account.route('/<int:account_id>/points/', methods=['POST'])
def account_level_point_add(account_id):
    params = s_param(userId=account_id)
    points = params.get('points')
    account_result = s_request('/account/level/details', {'id': account_id})
    current_points = account_result['entity']['point']
    diff = int(points) - int(current_points)
    if diff > 0:
        json_result = s_request('/account/level/add-point',
                                {'userId': account_id, 'point': abs(diff), 'remark': ''},
                                method='POST')
    elif diff < 0:
        json_result = s_request('/account/level/reduce-point',
                                {'userId': account_id, 'point': abs(diff), 'remark': ''},
                                method='POST')
    else:
        json_result = {}
    return s_response(json_result)


@account.route('/<int:account_id>/treasures/')
def account_billing_treasure(account_id):
    params = s_param()
    params['user_id'] = account_id
    json_result = s_request('/treasure/diamond/basic-info', params, method='GET')
    result = json_result.get('response')
    return s_response(result)


@account.route('/<int:account_id>/abuse/')
def account_abuse(account_id):
    params = s_param(page_param='pageNumber', page_size_param="pageSize")
    params['targetId'] = account_id
    json_result = s_request('/abuse/detail', params)
    result = json_result['response']['result']
    return s_response(result)


@account.route('/<int:account_id>/abuse/clear/', methods=['POST'])
def account_abuse_clear(account_id):
    params = {
        'targetId': account_id,
        'mark': 0
    }
    json_result = s_request('/abuse/clear', params, method='POST')
    return s_response(json_result)


@account.route('/<int:account_id>/recommend/', methods=['POST'])
def account_recommend(account_id):
    params = s_param(weight=0, expire_date=arrow.utcnow().timestamp)
    params['host_id'] = account_id
    s_request('/nbs/recommend', params, method='POST')
    return s_response({})


@account.route('/<int:account_id>/un-recommend/', methods=['POST'])
def account_unrecommend(account_id):
    params = s_param(weight=0, expire_date=arrow.utcnow().timestamp)
    params['host_id'] = account_id
    s_request('/nbs/unrecommend', params, method='POST')
    return s_response({})


@account.route('/<int:account_id>/is-banned/', methods=['GET'])
def account_is_banned(account_id):
    params = s_param()
    params['id'] = account_id
    json_response = s_request('/ban/isBanned', params, method='GET')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/ban/', methods=['GET'])
def account_ban_detail(account_id):
    params = s_param(id=account_id)
    json_response = s_request('/ban/detail', params, method='GET')
    result = json_response['response']
    return s_response(result)


@account.route('/<int:account_id>/ban/', methods=['POST'])
def account_ban(account_id):
    params = s_param()
    params['id'] = account_id
    json_response = s_request('/ban/setBan', params, method='POST')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/un-ban/', methods=['POST'])
def account_un_ban(account_id):
    params = s_param()
    params['id'] = account_id
    json_response = s_request('/ban/setUnBan', params, method='POST')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/income/')
def account_income(account_id):
    params = s_param()
    params['user_id'] = account_id
    json_response = s_request('/treasure/income/history', params, method='GET')
    previous_month = json_response['response']['incomePreviousMonth']
    current_month = json_response['response']['incomeCurrentMonth']

    if current_month:
        previous_month.push(current_month)
    result = previous_month
    return s_response(result)


@account.route('/<int:account_id>/cash-out/')
def account_cash_out(account_id):
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    params['userId'] = account_id
    json_response = s_request('/treasure/cash-out/details', params, method='GET')
    result, count = json_response['response']['result'], json_response['response']['total_record_count']
    return s_response(result, item_count=count)


@account.route('/<int:account_id>/diamonds-add/')
def account_diamonds_add(account_id):
    params = s_param()
    params['userId'] = account_id
    params['operator'] = 'admin'
    json_response = s_request('/treasure/reward-diamonds', params, method='POST')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/follower-count/', methods=['GET'])
def account_follower_count(account_id):
    params = s_param()
    params['userId'] = account_id
    json_response = s_request('/friendship/follower-count', params, method='GET')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/following-count/', methods=['GET'])
def account_following_count(account_id):
    params = s_param()
    params['userId'] = account_id
    json_response = s_request('/friendship/following-count', params, method='GET')
    result = json_response['response']
    return s_response({'msg': result})


@account.route('/<int:account_id>/privileges/', methods=['GET'])
def account_privileges(account_id):
    params = s_param()
    params['userId'] = account_id
    json_response = s_request('/privilege/get-privileges-status', params, method='GET')
    result = json_response
    return s_response(result)


@account.route('/<int:account_id>/privileges/', methods=['POST'])
def account_set_privileges(account_id):
    params = s_param()
    params['userId'] = account_id
    if 'enteringRoomAnimation' in params and 'enteringRoomEffect' in params:
        s_request('/privilege/set-entering-room-animation', params, method='POST')
        s_request('/privilege/set-entering-room-effect', params, method='POST')
    elif 'enteringRoomAnimation' in params and 'enteringRoomEffect' not in params:
        s_request('/privilege/set-entering-room-animation', params, method='POST')
    elif 'enteringRoomAnimation' not in params and 'enteringRoomEffect' in params:
        s_request('/privilege/set-entering-room-effect', params, method='POST')
    return s_response({'msg': ''})


@account.route('/<int:account_id>/block/', methods=['POST'])
def account_block(account_id):
    params = s_param(user_id=account_id)
    json_response = s_request('/block/block', params, method='POST')
    return s_response(json_response)


@account.route('/<int:account_id>/un-block/', methods=['POST'])
def account_un_block(account_id):
    params = s_param(user_id=account_id)
    json_response = s_request('/block/unblock', params, method='POST')
    return s_response(json_response)


# TODO not using right now
@account.route('/<int:account_id>/block/', methods=['GET'])
def account_block_info(account_id):
    params = s_param(user_id=account_id)
    json_response = s_request('/block/isBlocked', params, method='GET')
    return s_response(json_response)
