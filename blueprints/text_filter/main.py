from flask import Blueprint, jsonify

from utils.base_urls import s_request, s_param, s_response

text_filter = Blueprint('text_filter', __name__, url_prefix="/text-filter")


@text_filter.route('/', methods=['GET'])
def text_filter_list():
    params = s_param(page_param='pageNo', page_size_param='pageSize')
    json_response = s_request('/text-filter/get-pagination-words', params)
    result, count = json_response.get('wordList'), json_response.get('totalRecordCount')
    return s_response(result, item_count=count)


@text_filter.route('/', methods=['POST'])
def text_filter_create():
    params = s_param()
    params['words'] = params.get('word')
    json_response = s_request('/text-filter/add-words', params, method='POST')
    json_response.get('message')
    return s_response()


@text_filter.route('/<string:word>/', methods=['DELETE'])
def text_filter_delete(word):
    params = s_param(words=word)
    json_response = s_request('/text-filter/remove-words', params, method='POST')
    json_response.get('message')
    return s_response()
