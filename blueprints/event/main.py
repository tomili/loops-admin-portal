import uuid

from flask import Blueprint, jsonify

from utils.base_urls import s_param, s_request, s_response

event = Blueprint('event', __name__, url_prefix="/event")


@event.route('/', methods=['GET'])
def event_list():
    params = s_param(page_param='pageNumber', page_size_param='pageSize')
    json_result = s_request('/event/index', params, method='POST')
    result, count = json_result['response']['result'], json_result['response']['total_record_count']
    return s_response(result, item_count=count)


@event.route('/', methods=['POST'])
def event_create():
    params = s_param(event_id=uuid.uuid4().hex)
    json_result = s_request('/event/create', params, method='POST')
    result = json_result
    return jsonify(result)


@event.route('/<string:event_id>/', methods=['GET'])
def event_detail(event_id):
    params = s_param(id=event_id)
    json_result = s_request('/event/detail', params)
    return jsonify(json_result['response'])


@event.route('/<string:event_id>/', methods=['DELETE'])
def event_delete(event_id):
    params = {
        'id': event_id
    }
    s_request('/event/delete', params, method='POST')
    return jsonify({})


@event.route('/<string:event_id>/', methods=['PUT'])
def event_edit(event_id):
    params = s_param(id=event_id)
    json_result = s_request('/event/update', params, method='POST')
    result = json_result
    return jsonify(result)


@event.route('/<string:event_id>/recommend/', methods=['GET'])
def event_recommend(event_id):
    params = s_param(event_id=event_id, weight=0)
    json_response = s_request('/topevent/update', params, method='POST')
    result = json_response['response']
    return jsonify({'msg': result})
