from flask import Blueprint

from utils.base_urls import s_request, s_param, s_response

block = Blueprint('block', __name__, url_prefix='/block')


@block.route('/')
def blocked_user_list():
    params = s_param()
    json_response = s_request('/block/index', params)
    return s_response(json_response)
