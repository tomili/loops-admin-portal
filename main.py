from flask import Flask
from flask_cors import CORS
from utils.Schedulers import scheduler
from utils import logger
from utils.base_urls import s_response
from requests import ConnectionError
import logging

from blueprints.banner.main import banner
from blueprints.admin.main import admin
from blueprints.operation_log.main import operation_log
from blueprints.account.main import account
from blueprints.live.main import live
from blueprints.event.main import event
from blueprints.ban.main import ban
from blueprints.billing.main import transaction
from blueprints.tag.main import tag
from blueprints.text_filter.main import text_filter
from blueprints.recommended_event.main import recommended_event
from blueprints.reported_user.main import reported_user
from blueprints.feedback.main import feedback
from blueprints.photo.main import photo
from consts import DEFAULT_HEADERS
from init import setup_logger_handler

app = Flask(__name__)
app.register_blueprint(admin)
app.register_blueprint(operation_log)
app.register_blueprint(account)
app.register_blueprint(live)
app.register_blueprint(event)
app.register_blueprint(banner)
app.register_blueprint(ban)
app.register_blueprint(transaction)
app.register_blueprint(tag)
app.register_blueprint(text_filter)
app.register_blueprint(recommended_event)
app.register_blueprint(reported_user)
app.register_blueprint(feedback)
app.register_blueprint(photo)
CORS(app, expose_headers=DEFAULT_HEADERS.values())

app.logger.addHandler(setup_logger_handler())
app.logger.setLevel(logging.DEBUG)


@app.route('/')
def index():
    return s_response({'msg': 'here is admin portal api server'})


@app.errorhandler(ConnectionError)
def connection(e):
    logger.error(e)
    return s_response({}, error_code=1, error_message=e.message)


@app.errorhandler(Exception)
def unhandled_exception_handler(e):
    logger.error(e)
    return s_response({}, error_code=1, error_message=e.message)


@app.errorhandler(LookupError)
def lookup_handler(e):
    logger.error(e)
    return s_response({}, error_code=1, error_message=e.message)


scheduler.start()

if __name__ == "__main__":
    app.run()
