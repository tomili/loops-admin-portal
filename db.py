import logging
import redis
from playhouse.pool import PooledMySQLDatabase
from settings import settings
from playhouse.shortcuts import RetryOperationalError


class RetryDB(RetryOperationalError, PooledMySQLDatabase):
    """
        Auto retry db connection
        void 2006 Mysql have gone away
    """

    def sequence_exists(self, seq):
        pass


myDB = RetryDB(settings.DB_DATABASE,
               host=settings.DB_HOST,
               port=settings.DB_PORT,
               user=settings.DB_USER,
               passwd=settings.DB_PASSWORD)

logger = logging.getLogger('peewee')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

# use for cache user by token -> value
redis = redis.StrictRedis(db=0)
