import requests
import json
from settings import settings
from collections import Iterable
from flask import request, make_response, jsonify, Response
from peewee import Query, Model
from playhouse.shortcuts import model_to_dict
from consts import DEFAULT_HEADERS
from utils import strings, logger

ADMIN_SERVER = settings.ADMIN_SERVER
PHOTO_SERVER = settings.PHOTO_SERVER
PHOTO_DOMAIN = settings.PHOTO_DOMAIN


def s_param(page_param=None, page_size_param=None, **kwargs):
    """
    get all request params and change some name
    :param page_param:
    :param page_size_param:
    :return:
    """
    if request.method == 'GET':
        d = request.values.to_dict()
    else:
        d = request.json
    if 'page' in d and page_param:
        d[page_param] = d.get('page', None)
        d.pop('page', None)
    if 'page_size' in d and page_size_param:
        d[page_size_param] = d.get('page_size', None)
        d.pop('page_size')
    # make sure default value will be override
    return dict(kwargs.items() + d.items())


def s_request(path, params=None, method='GET', server=ADMIN_SERVER):
    """
    Forward the request to a certain URL to backend.

    :param server:
    :param path: endpoint of remote server
    :param params: params
    :param method: request methods
    :return: json result
    """
    if method not in ['GET', 'POST', 'DELETE', 'PUT', 'PATCH']:
        print 'method {} is not allowed'.format(method)
        raise requests.ConnectionError()

    logger.info(server + path + str(params))

    response = json.loads(requests.request(method, server + path, params=params, json=params).content)
    if 'success' in response and not strings.to_bool(response['success']):
        print response
        raise requests.ConnectionError
    return response


def s_upload(path, files, server=ADMIN_SERVER):
    response = json.loads(requests.post(server + path, files).content)
    return response


def s_response(model_value=None, item_count=None, error_code=0, error_message=''):
    """
    helper for response
    1. response is a list. will put Item-Count to header 
    2. response is a entity. will do nothing.
    3. response is a value. will wrap as { 'msg': <value> }
    
    :param error_code:
    :param error_message:
    :param item_count:
    :param model_value:
    :return: 
    """
    if isinstance(model_value, Query) or isinstance(model_value, Model):
        model_value = __handle_model__(model_value)

    if not isinstance(model_value, Response):
        model_value = jsonify(model_value)

    if error_message:
        error_code = 1

    response = make_response(model_value)
    if item_count:
        response.headers[DEFAULT_HEADERS.get('page-count')] = item_count
    if error_message:
        response.headers[DEFAULT_HEADERS.get('error-message')] = error_message
    if error_code:
        response.headers[DEFAULT_HEADERS.get('error-code')] = error_code

    return response


def __handle_model__(model_value):
    if isinstance(model_value, Iterable):
        if model_value:
            response_data = jsonify(map(lambda x: model_to_dict(x), model_value))
        else:
            response_data = jsonify([])
    else:
        if model_value:
            response_data = jsonify(model_to_dict(model_value))
        else:
            response_data = jsonify({})
    return response_data
