import json

from flask import jsonify, request, g
from functools import wraps

from playhouse.shortcuts import model_to_dict

from db import redis
from blueprints.admin.model import Admin


def login_required(f):
    """
    check user header in request.
    1. check user by token in redis
    2. check user by token in db
    3. return value
    :param f: route
    :return: 401 if not exist
    """

    @wraps(f)
    def wrapped(*args, **kwargs):
        header_token = request.headers.get('Authorization')
        if not header_token:
            return jsonify({'msg': 'you have not authenticated to request this resource'}), 401
        user = redis.get(header_token)
        if user:
            print user
            g._current_user_ = json.loads(user, encoding='UTF-8')
            return f(*args, **kwargs)

        user = Admin.get(
            token=header_token
        )
        if user:
            print 'in db'
            g._current_user_ = model_to_dict(user)
            return f(*args, **kwargs)

        return jsonify({}), 401

    return wrapped
