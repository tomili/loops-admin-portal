def to_bool(string):
    return string in ['true', 'True', '1', 1]
