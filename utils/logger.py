from datetime import datetime
from flask import g, current_app


def debug(msg):
    current_app.logger.debug(msg)


def info(msg):
    operation_log(msg)
    current_app.logger.info(msg)


def warn(msg):
    current_app.logger.warning(msg)


def error(msg):
    print type(msg)
    current_app.logger.error(msg)


def operation_log(msg, operation_type=None, target_id=None, target_type=None, **kwargs):
    from blueprints.operation_log.model import OperationLog
    if '_current_user_' in g or hasattr(g, '_current_user_'):
        user = g._current_user_
        OperationLog.create(
            operator=user['id'],
            detail=msg,
            create_time=datetime.now()
        )
