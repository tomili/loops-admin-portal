from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from flask import json
from playhouse.shortcuts import model_to_dict
from blueprints.admin.model import Admin
from db import redis
import atexit

scheduler = BackgroundScheduler()


def cache_user_in_redis():
    counter = 0
    for item in Admin.select():
        if item.token:
            redis.set(item.token, json.dumps(model_to_dict(item)))
            counter = counter + 1
    print 'cached {} entities'.format(counter)


scheduler.add_job(
    func=cache_user_in_redis,
    trigger=IntervalTrigger(minutes=1),
    name="cache user every minutes",
)

atexit.register(lambda: scheduler.running and scheduler.shutdown())
