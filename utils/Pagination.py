from flask import request
from consts import DEFAULT_PARAMETERS
from utils import logger


def add_pagination_params(query):
    page_param = DEFAULT_PARAMETERS.get('page_parameter_name')
    page_size_param = DEFAULT_PARAMETERS.get('page_size_parameter_name')

    page = int(request.values.get(page_param, 1))
    page_size = int(request.values.get(page_size_param, 20))

    total_count = query.count()
    return query.paginate(page, page_size), total_count


def add_filter_params(query, model):
    valid_request_keys = set(request.values.keys()) - set(DEFAULT_PARAMETERS.values())
    valid_filter_keys = filter(lambda x: hasattr(model, x), valid_request_keys)

    query_criterion = []
    for key in valid_filter_keys:
        query_criterion.append(getattr(model, key) == request.values.get(key))

    if not query_criterion:
        return query

    return query.where(*query_criterion)


def add_sort_params(query, model):
    sort_param = DEFAULT_PARAMETERS.get('sort_parameter_name')
    sort_order_param = DEFAULT_PARAMETERS.get('sort_order_parameter_name')

    sort_attr = request.values.get(sort_param)
    sort_order = request.values.get(sort_order_param, 'asc')

    if not sort_attr:
        return query

    if sort_order not in ['desc', 'asc']:
        logger.error('invalid sort order param, only ["desc", "asc"] is acceptable')
        return query

    if not hasattr(model, sort_attr):
        logger.error('invalid sort param {} on model {}'.format(sort_attr, model.__name__))
        return query

    return query.order_by(getattr(getattr(model, sort_attr), sort_order)())
