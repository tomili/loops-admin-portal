#!/usr/bin/env bash

port=${1:-5000} # set default port to 5000
name='loops-admin-portal'
echo 'Restarting... at'${port}
ps -ef|grep ${name} |grep -v grep|awk '{print $2}'|xargs -n1 kill
gunicorn main:app -w 4 -b 0.0.0.0:${port} -n ${name} --access-logfile /tmp/${name}-access.log --error-logfile /tmp/${name}-error.log -e env=product -D
echo 'Started'