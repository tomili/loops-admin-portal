import logging
import sys


# TODO Not using for now
def setup_db(message):
    """

    :param message:
    :return: promotion result
    """
    bash_input = raw_input(message + ' [Y/N]:')
    input_valid = bash_input.upper() in ['YES', 'NO', 'Y', 'N']
    if not input_valid:
        print 'Input invalid.'
        setup_db(message)

    return bash_input in ['YES', 'Y']


def setup_logger_handler():
    logging_handler_formatter = logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    )
    logging_handler = logging.StreamHandler(sys.stdout)
    logging_handler.setFormatter(logging_handler_formatter)
    return logging_handler
